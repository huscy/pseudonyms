# major.minor.patch
VERSION = (1, 2, 2)

__version__ = '.'.join(str(x) for x in VERSION)
