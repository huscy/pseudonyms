from itertools import cycle

import pytest
from model_bakery import baker

from huscy.pseudonyms import services

pytestmark = pytest.mark.django_db


def test_get_pseudonym(mocker, pseudonym):
    create_pseudonym_spy = mocker.spy(services, 'create_pseudonym')
    get_pseudonym_spy = mocker.spy(services, 'get_pseudonym')

    result = services.get_or_create_pseudonym(pseudonym.subject, pseudonym.content_type)

    assert result == pseudonym
    assert create_pseudonym_spy.call_count == 0
    assert get_pseudonym_spy.call_count == 1


def test_create_pseudonym(mocker, subject, content_type):
    create_pseudonym_spy = mocker.spy(services, 'create_pseudonym')
    get_pseudonym_spy = mocker.spy(services, 'get_pseudonym')

    services.get_or_create_pseudonym(subject, content_type)

    assert create_pseudonym_spy.call_count == 1
    assert get_pseudonym_spy.call_count == 1


def test_get_pseudonym_with_content_type_as_string(mocker, pseudonym, subject, content_type):
    get_pseudonym_spy = mocker.spy(services, 'get_pseudonym')

    services.get_or_create_pseudonym(subject, 'auth.permission')

    get_pseudonym_spy.assert_called_with(subject, content_type, None)


def test_create_pseudonym_with_content_type_as_string(mocker, subject, content_type):
    create_pseudonym_spy = mocker.spy(services, 'create_pseudonym')

    services.get_or_create_pseudonym(subject, 'auth.permission')

    create_pseudonym_spy.assert_called_with(subject, content_type, None)


def test_get_with_object_id(mocker, subject, content_type):
    create_pseudonym_spy = mocker.spy(services, 'create_pseudonym')
    get_pseudonym_spy = mocker.spy(services, 'get_pseudonym')

    pseudonyms = baker.make('pseudonyms.Pseudonym', subject=subject, content_type=content_type,
                            object_id=cycle([1, 2, 3]), _quantity=3)

    result = services.get_or_create_pseudonym(subject, content_type, 2)

    assert result != pseudonyms[0]
    assert result == pseudonyms[1]
    assert result != pseudonyms[2]
    assert create_pseudonym_spy.call_count == 0
    assert get_pseudonym_spy.call_count == 1


def test_create_with_object_id(mocker, subject, content_type):
    create_pseudonym_spy = mocker.spy(services, 'create_pseudonym')
    get_pseudonym_spy = mocker.spy(services, 'get_pseudonym')

    pseudonyms = baker.make('pseudonyms.Pseudonym', subject=subject, content_type=content_type,
                            object_id=cycle([1, 2, 3]), _quantity=3)

    pseudonym = services.get_or_create_pseudonym(subject, content_type, 4)

    assert pseudonym not in pseudonyms
    assert create_pseudonym_spy.call_count == 1
    assert get_pseudonym_spy.call_count == 1
