import pytest

from huscy.pseudonyms.services import get_subject

pytestmark = pytest.mark.django_db


def test_get_subject(pseudonym):
    result = get_subject('123456789')

    assert result == pseudonym.subject


def test_max_num_of_queries(django_assert_max_num_queries, pseudonym):
    with django_assert_max_num_queries(1):
        get_subject('123456789')
