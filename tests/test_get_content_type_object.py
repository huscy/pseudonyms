import pytest

from django.contrib.contenttypes.models import ContentType

from huscy.pseudonyms.services import _get_content_type_object

pytestmark = pytest.mark.django_db


def test_pass_content_type_object(content_type):
    result = _get_content_type_object(content_type)

    assert isinstance(result, ContentType)
    assert result.app_label == 'auth'
    assert result.model == 'permission'


def test_pass_string():
    result = _get_content_type_object('auth.permission')

    assert isinstance(result, ContentType)
    assert result.app_label == 'auth'
    assert result.model == 'permission'


@pytest.mark.parametrize('content_type, data_type', [
    (None, 'NoneType'),
    (1, 'int'),
    ((1,), 'tuple'),
    ([], 'list'),
    ({}, 'dict'),
])
def test_pass_not_supported_type(content_type, data_type):
    with pytest.raises(Exception) as error:
        _get_content_type_object(content_type)

    assert str(error.value) == f'Type {data_type} not supported.'
