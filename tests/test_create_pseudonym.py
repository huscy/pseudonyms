import pytest

from huscy.pseudonyms.services import create_pseudonym

pytestmark = pytest.mark.django_db


def test_create_pseudonym(subject, content_type):
    result = create_pseudonym(subject, content_type)

    assert result.subject == subject
    assert len(str(result.code)) == 32
    assert result.content_type == content_type
    assert result.object_id is None


def test_create_pseudonym_with_content_type_as_string(subject, content_type):
    result = create_pseudonym(subject, 'auth.permission')

    assert result.subject == subject
    assert result.content_type == content_type
    assert result.object_id is None


def test_with_duplicated_code(mocker, pseudonym):
    generate_code_mock = mocker.patch('huscy.pseudonyms.services._generate_code')
    # return '123456789' on first call and 'pseudonym' on second call of _generate_code()
    generate_code_mock.side_effect = ['123456789', 'pseudonym']

    result = create_pseudonym(pseudonym.subject, pseudonym.content_type)

    assert result.code == 'pseudonym'
    assert generate_code_mock.call_count == 2


def test_with_object_id(subject, content_type):
    pseudonym1 = create_pseudonym(subject, content_type, 1)
    pseudonym2 = create_pseudonym(subject, content_type, 2)

    assert pseudonym1.code != pseudonym2.code
    assert pseudonym1.object_id == 1
    assert pseudonym2.object_id == 2
    assert pseudonym1.subject == pseudonym2.subject
