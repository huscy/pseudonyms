from itertools import cycle
from operator import attrgetter

import pytest
from model_bakery import baker

from huscy.pseudonyms.services import get_subjects

pytestmark = pytest.mark.django_db


@pytest.fixture
def subjects():
    return baker.make('subjects.Subject', _quantity=6)


@pytest.fixture
def pseudonyms(subjects):
    return baker.make('pseudonyms.Pseudonym', subject=cycle(subjects), _quantity=3)


def test_get_subjects(subjects, pseudonyms):
    results = get_subjects([pseudonym.code for pseudonym in pseudonyms])

    assert list(results.order_by('id')) == sorted(subjects[:3], key=attrgetter('id'))


def test_get_subjects_with_multiple_pseudonyms(subjects, pseudonyms):
    pseudonyms.extend(baker.make('pseudonyms.Pseudonym', subject=cycle(subjects[:2]), _quantity=2))

    results = get_subjects([pseudonym.code for pseudonym in pseudonyms])

    assert list(results.order_by('id')) == sorted(subjects[:3], key=attrgetter('id'))
