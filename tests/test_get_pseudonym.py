from itertools import cycle

import pytest
from model_bakery import baker

from huscy.pseudonyms.models import Pseudonym
from huscy.pseudonyms.services import get_pseudonym

pytestmark = pytest.mark.django_db


def test_get_pseudonym(pseudonym):
    result = get_pseudonym(pseudonym.subject, pseudonym.content_type)

    assert result == pseudonym


def test_get_pseudonym_with_content_type_as_string(pseudonym):
    result = get_pseudonym(pseudonym.subject, 'auth.permission')

    assert result == pseudonym


def test_get_with_object_id(content_type, subject):
    pseudonyms = baker.make(Pseudonym, subject=subject, content_type=content_type,
                            object_id=cycle([1, 2, 3]), _quantity=3)

    pseudonym = get_pseudonym(subject, content_type, 2)

    assert pseudonym != pseudonyms[0]
    assert pseudonym == pseudonyms[1]
    assert pseudonym != pseudonyms[2]


@pytest.mark.parametrize('object_id', [None, 3])
def test_pseudonym_does_not_exist(subject, content_type, object_id):
    with pytest.raises(Pseudonym.DoesNotExist):
        get_pseudonym(subject, content_type, object_id)
