import pytest
from model_bakery import baker

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from huscy.pseudonyms.models import Pseudonym


@pytest.fixture
def user():
    return User.objects.create_user(username='user', password='123',
                                    first_name='Ken', last_name='Guru')


@pytest.fixture
def subject():
    return baker.make('subjects.Subject')


@pytest.fixture
def content_type():
    return ContentType.objects.get_by_natural_key('auth', 'permission')


@pytest.fixture
def pseudonym(subject, content_type):
    return Pseudonym.objects.create(subject=subject, code='123456789', content_type=content_type)
